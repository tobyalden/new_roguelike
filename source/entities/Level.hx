package entities;

import haxepunk.*;
import haxepunk.graphics.*;
import haxepunk.graphics.tile.*;
import haxepunk.masks.*;
import haxepunk.math.*;
import openfl.Assets;

class Level extends Entity
{
    public static inline var TILE_SIZE = 10;
    public static inline var LEVEL_WIDTH_IN_TILES = 32;
    public static inline var LEVEL_HEIGHT_IN_TILES = 18;

    private var walls:Grid;
    private var tiles:Tilemap;
    public var entities(default, null):Array<MiniEntity>;

    public function new(levelName:String) {
        super(0, 0);
        type = "walls";
        loadLevel(levelName);
        updateGraphic();
        mask = walls;
    }

    override public function update() {
        super.update();
    }

    private function loadLevel(levelName:String) {
        //var xml = new haxe.xml.Access(Xml.parse(Assets.getText('levels/${levelName}.tmx')));
        var levelData = haxe.Json.parse(Assets.getText('levels/${levelName}.json'));

        var mapSize = 4;

        // Create grid
        walls = new Grid(
            TILE_SIZE * LEVEL_WIDTH_IN_TILES * mapSize,
            TILE_SIZE * LEVEL_HEIGHT_IN_TILES * mapSize,
            TILE_SIZE,
            TILE_SIZE
        );

        for(mapX in 0...mapSize) {
            for(mapY in 0...mapSize) {
                var segmentWalls = new Grid(
                    TILE_SIZE * LEVEL_WIDTH_IN_TILES,
                    TILE_SIZE * LEVEL_HEIGHT_IN_TILES,
                    TILE_SIZE,
                    TILE_SIZE
                );
                for(layerIndex in 0...levelData.layers.length) {
                    var layer = levelData.layers[layerIndex];
                    if(layer.name == "walls") {
                        for(tileY in 0...layer.grid2D.length) {
                            for(tileX in 0...layer.grid2D[0].length) {
                                segmentWalls.setTile(tileX, tileY, layer.grid2D[tileY][tileX] == "1");
                            }
                        }
                    }
                    if(layer.name == "entities") {
                        for(entityIndex in 0...layer.entities.length) {
                            var entity = layer.entities[entityIndex];
                            if(entity.name == "optionalSolid") {
                                if(Random.random > 0.5) {
                                    //continue;
                                }
                                var tileStartX = Std.int(entity.x / TILE_SIZE);
                                var tileStartY = Std.int(entity.y / TILE_SIZE);
                                var tileWidth = Std.int(entity.width / TILE_SIZE);
                                var tileHeight = Std.int(entity.height / TILE_SIZE);
                                for(tileX in 0...tileWidth) {
                                    for(tileY in 0...tileHeight) {
                                        segmentWalls.setTile(tileStartX + tileX, tileStartY + tileY, true);
                                    }
                                }
                            }
                        }
                    }
                }
                if(Random.random > 0.5) {
                    flipHorizontally(segmentWalls);
                }
                for(tileX in 0...segmentWalls.columns) {
                    for(tileY in 0...segmentWalls.rows) {
                        walls.setTile(
                            mapX * LEVEL_WIDTH_IN_TILES + tileX, 
                            mapY * LEVEL_HEIGHT_IN_TILES + tileY,
                            segmentWalls.getTile(tileX, tileY)
                        );
                    }
                }
            }
        }

        // Seal map
        for(tileX in 0...walls.columns) {
            for(tileY in 0...walls.rows) {
                if(tileX == 0 || tileY == 0 || tileX == walls.columns - 1 || tileY == walls.rows - 1) {
                    walls.setTile(tileX, tileY, true);
                }
            }
        }

        // Add entities
        entities = new Array<MiniEntity>();

        var emptyTiles = [];
        for(tileX in 0...walls.columns) {
            for(tileY in 0...walls.rows) {
                if(!walls.getTile(tileX, tileY)) {
                    emptyTiles.push({tileX: tileX, tileY: tileY});
                }
                if(
                    walls.getTile(tileX, tileY)
                    && !walls.getTile(tileX, tileY - 1)
                    && !walls.getTile(tileX - 1, tileY)
                    && !walls.getTile(tileX - 1, tileY - 1)
                    && !walls.getTile(tileX - 1, tileY + 1)
                    && !walls.getTile(tileX - 1, tileY + 2)
                ) {
                    var handhold = new Handhold(tileX * TILE_SIZE, tileY * TILE_SIZE);
                    handhold.moveBy(-handhold.width / 2, -handhold.height / 2);
                    entities.push(handhold);
                }
                if(
                    walls.getTile(tileX, tileY)
                    && !walls.getTile(tileX, tileY - 1)
                    && !walls.getTile(tileX + 1, tileY)
                    && !walls.getTile(tileX + 1, tileY - 1)
                    && !walls.getTile(tileX + 1, tileY + 1)
                    && !walls.getTile(tileX + 1, tileY + 2)
                ) {
                    var handhold = new Handhold((tileX + 1) * TILE_SIZE, tileY * TILE_SIZE);
                    handhold.moveBy(-handhold.width / 2, -handhold.height / 2);
                    entities.push(handhold);
                }
            }
        }
        HXP.shuffle(emptyTiles);

        var player = new Player(emptyTiles[0].tileX * TILE_SIZE, emptyTiles[0].tileY * TILE_SIZE);
        entities.push(player);
    }

    public function flipHorizontally(wallsToFlip:Grid) {
        for(tileX in 0...Std.int(wallsToFlip.columns / 2)) {
            for(tileY in 0...wallsToFlip.rows) {
                var tempLeft:Null<Bool> = wallsToFlip.getTile(tileX, tileY);
                // For some reason getTile() returns null instead of false!
                if(tempLeft == null) {
                    tempLeft = false;
                }
                var tempRight:Null<Bool> = wallsToFlip.getTile(
                    wallsToFlip.columns - tileX - 1, tileY
                );
                if(tempRight == null) {
                    tempRight = false;
                }
                wallsToFlip.setTile(tileX, tileY, tempRight);
                wallsToFlip.setTile(
                    wallsToFlip.columns - tileX - 1, tileY, tempLeft
                );
            }
        }
    }

    public function updateGraphic() {
        tiles = new Tilemap(
            'graphics/tiles.png',
            walls.width, walls.height, TILE_SIZE, TILE_SIZE
        );
        for(tileX in 0...walls.columns) {
            for(tileY in 0...walls.rows) {
                if(walls.getTile(tileX, tileY)) {
                    tiles.setTile(tileX, tileY, 0);
                }
            }
        }
        graphic = tiles;
    }
}

