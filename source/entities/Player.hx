package entities;

import haxepunk.*;
import haxepunk.graphics.*;
import haxepunk.input.*;
import haxepunk.masks.*;
import haxepunk.math.*;
import haxepunk.Tween;
import haxepunk.tweens.misc.*;
import haxepunk.tweens.motion.*;
import haxepunk.utils.*;
import scenes.*;
import openfl.Assets;

typedef PlayerAttributes = {
    var runAccel:Float;
    var airAccel:Float;
    var maxRunSpeed:Float;
    var maxAirSpeed:Float;
    var jumpPower:Float;
    var jumpCancelPower:Float;
    var gravity:Float;
    var hangTimeGravityMultiplier:Float;
    var maxFallSpeed:Float;
    var wallScalePower:Float;
    var wallJumpPowerX:Float;
    var wallJumpPowerY:Float;
    var wallStickTime:Float;
    var wallScaleThreshold:Float;
    var wallScaleWindow:Float;
    var yankSpeed:Float;
    var yankUpwardsBoost:Float;
    var yankCooldown:Float;
}

class Player extends MiniEntity
{
    private var attributes:PlayerAttributes;

    private var sprite:Spritemap;
    private var velocity:Vector2;
    private var isHanging:Bool;
    private var timeOnWall:Float;
    private var timeWallStuck:Float;
    private var yankCooldown:Alarm;

    public function new(x:Float, y:Float) {
        super(x, y);
        name = "player";
        layer = -3;
        mask = new Hitbox(10, 20);

        attributes = haxe.Json.parse(
            Assets.getText("data/playerattributes.json")
        );
        sprite = new Spritemap("graphics/player.png", 10, 20);
        sprite.add("idle", [0]);
        sprite.play("idle");
        graphic = sprite;

        velocity = new Vector2();

        isHanging = false;
        timeOnWall = 0;
        timeWallStuck = 0;
        yankCooldown = new Alarm(attributes.yankCooldown);
        addTween(yankCooldown);
    }

    override public function update() {
        // Horizontal movement
        var accel = isOnGround() ? attributes.runAccel : attributes.airAccel;
        if(isHanging) {
            velocity.x = 0;
        }
        else if(Input.check("left")) {
            if(!(isOnRightWall() && !isOnGround() && timeWallStuck < attributes.wallStickTime)) {
                velocity.x -= accel * HXP.elapsed;
            }
        }
        else if(Input.check("right")) {
            if(!(isOnLeftWall() && !isOnGround() && timeWallStuck < attributes.wallStickTime)) {
                velocity.x += accel * HXP.elapsed;
            }
        }
        else {
            velocity.x = MathUtil.approach(velocity.x, 0, accel * HXP.elapsed);
        }
        var maxHorizontalSpeed = isOnGround() ? attributes.maxRunSpeed : attributes.maxAirSpeed;
        velocity.x = MathUtil.clamp(velocity.x, -maxHorizontalSpeed, maxHorizontalSpeed);

        // Vertical movement
        if(isOnGround() || isHanging) {
            velocity.y = 0;
        }
        else {
            if(Math.abs(velocity.y) < Math.abs(attributes.jumpCancelPower)) {
                velocity.y += attributes.gravity * attributes.hangTimeGravityMultiplier * HXP.elapsed;
            }
            else {
                velocity.y += attributes.gravity * HXP.elapsed;
            }
            if(Input.released("jump")) {
                velocity.y = Math.max(velocity.y, -attributes.jumpCancelPower);
            }
        }
        velocity.y = Math.min(velocity.y, attributes.maxFallSpeed);

        // Grab handholds
        var handhold = collide("handhold", x, y);
        if(handhold != null) {
            if(!isOnGround() && y <= handhold.centerY && y + velocity.y * HXP.elapsed >= handhold.centerY) {
                isHanging = true;
                moveTo(x, handhold.centerY);
                velocity.x = 0;
                velocity.y = 0;
            }
        }

        // Wall sticking
        if(isOnWall()) {
            timeOnWall += HXP.elapsed;
            if(isOnLeftWall() && Input.check("right") || isOnRightWall() && Input.check("left")) {
                timeWallStuck += HXP.elapsed;
            }
            else {
                timeWallStuck = 0;
            }
        }
        else {
            timeOnWall = 0;
        }

        // Jumping
        if(Input.pressed("jump")) {
            if(isOnGround()) {
                velocity.y = -attributes.jumpPower;
            }
            else if(isHanging) {
                isHanging = false;
                velocity.y = -attributes.jumpPower;
            }
            else if(isOnWall()) {
                if(
                    timeOnWall <= attributes.wallScaleWindow
                    && velocity.y <= attributes.wallScaleThreshold
                    && (
                        Input.check("left") && isOnLeftWall()
                        || Input.check("right") && isOnRightWall()
                    )
                ) {
                    velocity.y = -attributes.wallScalePower;
                }
                else if(
                    Input.check("right") && isOnLeftWall()
                    || Input.check("left") && isOnRightWall()
                ) {
                    velocity.x = (isOnLeftWall() ? 1 : -1) * attributes.wallJumpPowerX;
                    velocity.y = -attributes.wallJumpPowerY;
                }
            }
        }

        // Yanking
        if(Input.pressed("action") && !yankCooldown.active) {
            var closestHandhold = getClosestHandhold();
            if(closestHandhold != null) {
                yankTowardsHandhold(closestHandhold);
                yankCooldown.start();
                isHanging = false;
            }
        }

        super.update();

        moveBy(velocity.x * HXP.elapsed, velocity.y * HXP.elapsed, ["walls"]);

        animation();
    }

    private function animation() {
        if(Input.check("left")) {
            sprite.flipX = true;
        }
        else if(Input.check("right")) {
            sprite.flipX = false;
        }
    }

    override public function moveCollideX(e:Entity) {
        velocity.x = 0;
        return true;
    }

    override public function moveCollideY(e:Entity) {
        velocity.y = 0;
        return true;
    }

    private function yankTowardsHandhold(handhold:Handhold) {
        velocity.x = handhold.centerX - centerX;
        velocity.y = handhold.centerY - centerY;
        velocity.normalize(attributes.yankSpeed);
        velocity.y -= attributes.yankUpwardsBoost;
    }

    private function getClosestHandhold():Handhold {
        var closestHandhold:Handhold = null;
        for(handhold in cast(HXP.scene, GameScene).handholds) {
            if(
                handhold.isOnScreen()
                && HXP.scene.collideLine(
                    "walls",
                    Std.int(sprite.flipX ? left : right), Std.int(top),
                    Std.int(handhold.centerX), Std.int(handhold.centerY)
                ) == null
                && handhold.bottom < top
                && (
                    sprite.flipX && handhold.centerX < centerX
                    || !sprite.flipX && handhold.centerX > centerX
                )
            ) {
                if(
                    closestHandhold == null
                    || distanceFrom(handhold, true) < distanceFrom(closestHandhold, true)
                ) {
                    closestHandhold = handhold;
                }
            }
        }
        return closestHandhold;
    }

    override public function render(camera:Camera) {
        var closestHandhold = getClosestHandhold();
        if(closestHandhold != null) {
            Draw.color = 0xFFFFFF;
            Draw.alpha = yankCooldown.active ? 1 : 0.5;
            Draw.line(
                (sprite.flipX ? left : right) - HXP.scene.camera.x,
                top - HXP.scene.camera.y,
                closestHandhold.centerX - HXP.scene.camera.x,
                closestHandhold.centerY - HXP.scene.camera.y
            );
        }
        super.render(camera);
    }
}
