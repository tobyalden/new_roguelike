package entities;

import haxepunk.*;
import haxepunk.graphics.*;
import haxepunk.input.*;
import haxepunk.masks.*;
import haxepunk.math.*;
import haxepunk.Tween;
import haxepunk.tweens.misc.*;
import scenes.*;
import openfl.Assets;

class Handhold extends MiniEntity
{
    public function new(x:Float, y:Float) {
        super(x, y);
        type = "handhold";
        layer = -5;
        mask = new Hitbox(2, 2);
        graphic = new ColoredRect(width, height, 0x00FF00);
    }

    override public function update() {
        super.update();
    }
}
