package scenes;

import entities.*;
import haxepunk.*;
import haxepunk.graphics.*;
import haxepunk.graphics.tile.*;
import haxepunk.input.*;
import haxepunk.masks.*;
import haxepunk.math.*;
import haxepunk.Tween;
import haxepunk.tweens.misc.*;
import haxepunk.utils.*;
import openfl.Assets;

class GameScene extends Scene
{
    public var handholds(default, null):Array<Handhold>;
    private var level:Level;
    private var player:Player;

    override public function begin() {
        handholds = [];
        level = add(new Level("test_level"));
        for(entity in level.entities) {
            add(entity);
            if(entity.name == "player") {
                player = cast(entity, Player);
            }
            else if(entity.type == "handhold") {
                handholds.push(cast(entity, Handhold));
            }
        }
        //camera.scale = 0.25;
    }

    override public function update() {
        super.update();
        camera.x = player.x - HXP.width / 2;
        camera.y = player.y - HXP.height / 2;
    }
}
